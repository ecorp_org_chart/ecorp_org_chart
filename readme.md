# Ecorp org chart

## Installation

Suivre les instructions du [lien vers les donnees](#lien-vers-les-donnees-).

```shell
git clone git@gitlab.com:ecorp_org_chart/ecorp_org_chart.git
cd ecorp_org_chart

chmod +x ./install.sh
./install.sh

chmod +x ./run.sh
./run.sh
```

Le fichier json genere se trouve la ou on a lance le script.

## Brief

### Lien vers la consigne

[depot de l'exercice](https://gitlab.com/Nasyourte/ecorp-org)

### Lien vers les donnees :

[base de donnee de l'exercice](https://github.com/datacharmer/test_db)


## Attendu a la fin dans ce depot

Le rendu est uniquement des fichier python permettant de generer le json.


## Recherche

### Note

#### Date

il y a des dates. on considere comme actuel toutes les donnees qui ont comme valeur
dans ```to_date``` 9999-01-01.

#### Tree

Il est utile de trouver le chef de chaque employe a un instant t. il n'est pas 
necessaire de parcourir l'arbre pour cela. si pour chaque employe on a le chef
alors ceux qui ont pour chef eux meme, sont la racine de l'arbre.

### Les employees et leurs chef

#### Requete
```sql
SELECT     
    de.emp_no,     
    dm.emp_no 
FROM dept_emp AS de 
JOIN departments d ON d.dept_no = de.dept_no AND de.to_date = "9999-01-01"
JOIN dept_manager dm ON dm.dept_no = d.dept_no AND dm.to_date = "9999-01-01"; 
```

### Le titre de chaque employee

#### Requetes

```sql
SELECT 
    e.first_name AS prenom,
    e.last_name AS nom,
    t.title AS titre
FROM 
    employees e
JOIN 
    titles t ON e.emp_no = t.emp_no
WHERE 
    t.to_date = '9999-01-01';
```

#### Resultat

|prenom|nom|titre|
|:--|:--|:--|
| Toshimi        | Azadmanesh       | Senior Engineer    |
| Mitsuyuki      | Itschner         | Senior Engineer    |
| Lijie          | Furudate         | Senior Engineer    |
| Otilia         | Potthoff         | Senior Engineer    |
| Lijia          | Zultner          | Senior Engineer    |
| Gadiel         | Klyachko         | Senior Engineer    |
| Nahid          | Miara            | Engineer           |
| Kasturi        | Zucker           | Senior Staff       |
| Khatoun        | Range            | Senior Staff       |
| Debatosh       | Liesche          | Senior Engineer    |
| Rosine         | Trystram         | Senior Engineer    |
| Odinaldo       | Muntz            | Senior Staff       |
| Heeju          | Buescher         | Senior Staff       |
| Alper          | Peck             | Senior Staff       |
| Jeong          | Nourani          | Senior Engineer    |
| Ulf            | Edelhoff         | Senior Engineer    |
| Aloys          | Katalagarianos   | Senior Staff       |
| Alexius        | Eiter            | Senior Engineer    |
| Doohun         | Bade             | Senior Staff       |
| Mario          | Zschoche         | Senior Engineer    |
| Janche         | Henders          | Senior Staff       |
| Cristinel      | Plessier         | Senior Engineer    |
| Shietung       | Bergere          | Senior Staff       |
| Ortrun         | Callaway         | Senior Engineer    |
| Fox            | Kugler           | Senior Staff       |
| Nathan         | Chimia           | Senior Staff       |
| Zito           | Baaz             | Senior Engineer    |

### Nom et prenom de chacun

#### Requetes

J'essayé de chercher si quel q'un est traveilleur toujour.

```SQL
SELECT e.emp_no, e.first_name, e.last_name 
FROM employees e 
LEFT JOIN dept_emp de ON e.emp_no = de.emp_no 
GROUP BY e.emp_no, e.first_name, e.last_name 
HAVING MAX(de.to_date) = '9999-01-01';
```

#### Resultat

| emp_no | first_name     | last_name        |
|--------|----------------|------------------|
| 499983 | Uri            | Juneja           |
| 499984 | Kaijung        | Rodham           |
| 499985 | Gila           | Lukaszewicz      |
| 499986 | Nathan         | Ranta            |
| 499987 | Rimli          | Dusink           |
| 499990 | Khaled         | Kohling          |
| 499991 | Pohua          | Sichman          |
| 499992 | Siamak         | Salverda         |
| 499993 | DeForest       | Mullainathan     |
| 499995 | Dekang         | Lichtner         |
| 499996 | Zito           | Baaz             |
| 499997 | Berhard        | Lenart           |
| 499998 | Patricia       | Breugel          |
| 499999 | Sachin         | Tsukuda          |

#### Requetes

J'essayé concatener son first name avec last name.

```SQL
SELECT 
    e.emp_no, 
    CONCAT(e.first_name, ' ', e.last_name) AS full_name 
FROM employees e 
LEFT JOIN dept_emp de ON e.emp_no = de.emp_no 
GROUP BY e.emp_no, full_name 
HAVING MAX(de.to_date) = '9999-01-01';
```

#### Resultat

| emp_id | full_name                      |
|--------|--------------------------------|
| 499985 | Gila Lukaszewicz               |
| 499986 | Nathan Ranta                   |
| 499987 | Rimli Dusink                   |
| 499990 | Khaled Kohling                 |
| 499991 | Pohua Sichman                  |
| 499992 | Siamak Salverda                |
| 499993 | DeForest Mullainathan          |
| 499995 | Dekang Lichtner                |
| 499996 | Zito Baaz                      |
| 499997 | Berhard Lenart                 |
| 499998 | Patricia Breugel               |
| 499999 | Sachin Tsukuda                 |

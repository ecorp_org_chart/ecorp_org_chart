#!/bin/bash
# run the generator, populate environement variable with .env data
# and pass the parameters to the python script

SCRIPT_DIR=$(dirname $(realpath "$0"))
if [ -f "$SCRIPT_DIR/.env" ]; then
  export $(cat $SCRIPT_DIR/.env | xargs)
fi

echo "$@"
source "$SCRIPT_DIR/.venv/bin/activate"
python3 "$SCRIPT_DIR/generate.py" "$@"
deactivate

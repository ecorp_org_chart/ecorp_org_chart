import argparse
import json
import os

import mysql.connector


def get_data(conn, args) -> list[dict]:
    """
    Recupere les donnees necessaire pour la generation du json.

    :param cursor:
    :return: generator
    """
    query = """
    SELECT     
        de.emp_no AS id,     
        dm.emp_no AS pid,
        t.title,
        CONCAT(e.first_name, " ", e.last_name) AS name,
        s.salary
    FROM dept_emp AS de 
    JOIN departments d ON d.dept_no = de.dept_no AND de.to_date = "9999-01-01"
    JOIN dept_manager dm ON dm.dept_no = d.dept_no AND dm.to_date = "9999-01-01"
    JOIN employees e ON e.emp_no = de.emp_no
    JOIN titles t ON e.emp_no = t.emp_no AND t.to_date = "9999-01-01"
    JOIN salaries s ON e.emp_no = s.emp_no AND s.to_date = "9999-01-01" 
    """
    if "service" in args:
        query += " WHERE d.dept_name = %(service)s"
    if "limit" in args:
        query += " LIMIT %(limit)s"
    if "offset" in args:
        query += " OFFSET %(offset)s"
    cursor = conn.cursor(dictionary=True)
    if args:
        cursor.execute(query, args)
    else:
        cursor.execute(query)

    for row in cursor.fetchall():
        yield row

    cursor.close()
    conn.close()


def del_pid(row: dict) -> dict:
    """
    Enleve le pid de la ligne si l'id et le pid sont egaux
    (si un employe est le chef de lui meme)

    :param row: dict, ligne de la reponse a la requete
    :return: la meme ligne sans la clefs pid
    """
    if row['pid'] == row['id']:
        del row['pid']
    return row


def to_json_file(generator, minify=True):
    """
    encode et ecris dans un fichier json

    :param generator:
    """
    with open("data.json", "w") as f:
        f.write("[")
        if not minify: f.write("\n")
        for i, row in enumerate(generator):
            if i > 0:
                f.write(',')
                if not minify: f.write("\n")
            clean_row = del_pid(row)
            if not minify:
                f.write(json.dumps(clean_row, indent=4))
            else:
                f.write(json.dumps(clean_row))
        if not minify: f.write("\n")
        f.write("]\n")


def get_args() -> dict:
    """
    get arguments from command line

    :return: dictionary with arguments
    """
    parser = argparse.ArgumentParser(
        prog="run.sh",
        description="create a json from employees database to populate the organigram",
        epilog="Wouhou !!!"
    )
    parser.add_argument(
        "-l", "--limit",
        help="limite le nombre de reponse",
        type=int
    )
    parser.add_argument(
        "-s", "--service",
        help="Le service que l'on veut voir",
        type=str
    )
    parser.add_argument(
        "-o", "--offset",
        help="offset",
        type=int
    )
    return {k: v for k, v in parser.parse_args().__dict__.items() if v is not None}


if __name__ == '__main__':
    if not any(key in os.environ for key in ("DB_USER", "DB_PASSWORD", "DB_HOST")):
        print("les variables d'environnement ne sont pas chargees correctement")
        print("Veillez a lancer install.sh et utiliser le run.sh pour lancer le script")
        exit(1)

    to_json_file(
        get_data(
            mysql.connector.connect(
                user=os.environ["DB_USER"],
                password=os.environ["DB_PASS"],
                database="employees",
                host=os.environ["DB_HOST"]
            ),
            get_args()
        )
    )
